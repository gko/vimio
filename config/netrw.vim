let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_altv = 1

autocmd FileType netrw setl bufhidden=delete
