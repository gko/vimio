let g:rg_command = "--no-ignore --hidden --follow --files
    \ --glob '!{.git,node_modules,plugged}/**'
    \ --glob '*.{js,json,jsx,ts,tsx,php,md,styl,scss,sass,less,pug,html,config,py,rb,rs,cpp,c,go,hs,elm,java,scala,txt}'"
