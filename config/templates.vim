" initial templates for files
autocmd BufNewFile *.py 0r $VIMHOME/templates/template.py
autocmd BufNewFile *.xml 0r $VIMHOME/templates/template.xml
autocmd BufNewFile *.xsl 0r $VIMHOME/templates/template.xsl
autocmd BufNewFile *.jade 0r $VIMHOME/templates/template.jade
autocmd BufNewFile *.html 0r $VIMHOME/templates/template.html
autocmd BufNewFile package.json 0r $VIMHOME/templates/package.json
